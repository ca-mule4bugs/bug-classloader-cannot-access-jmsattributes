package com.canda.log;

import java.util.Map.Entry;

import org.mule.extension.http.api.HttpAttributes;
import org.mule.runtime.api.util.MultiMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * com.canda.mulestac.mule4.logging.optional.HttpAttributesResolver
 */
public class LogInfoHttp {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogInfoHttp.class);

	/**
	 *
	 */
	public static void logHttpAttributes(Object aAttributes) {
		if (aAttributes instanceof HttpAttributes) {
			HttpAttributes tempHttpAttributes = (HttpAttributes) aAttributes;
			MultiMap<String, String> tempHeaders = tempHttpAttributes.getHeaders();
			for (Entry<String, String> tempEntry : tempHeaders.entryList()) {
				String tempVarableName = tempEntry.getKey();
				LOGGER.info("httpAttr: " + tempVarableName + "=" + tempEntry.getValue());
			}
		} else {
			LOGGER.info("It is not " + HttpAttributes.class + " loaded via " + HttpAttributes.class.getClassLoader());
		}

	}

}
