package com.canda.log;

import org.mule.runtime.api.message.Message;
import org.mule.runtime.api.metadata.TypedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class LogInfo {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogInfo.class);

	public static void log(Message aMessage) {
		LOGGER.info("aMessage=" + aMessage);
		TypedValue<Object> tempAttributes = aMessage.getAttributes();
		LOGGER.info("tempAttributes=" + tempAttributes);
		Object tempValue = tempAttributes.getValue();
		LOGGER.info("tempAttributes.value=" + tempValue);
		if (tempValue != null) {
			LOGGER.info("tempAttributes.value.class=" + tempValue.getClass());
		}
		LOGGER.info("http...");
		LogInfoHttp.logHttpAttributes(tempValue);
		LOGGER.info("jms...");
		LogInfoJms.logJmsAttributes(tempValue);
		LOGGER.info("Finished.");

	}
}
