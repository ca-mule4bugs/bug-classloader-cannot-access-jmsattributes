package com.canda.log;

import java.util.Map;

import org.mule.jms.commons.api.message.JmsAttributes;
import org.mule.jms.commons.api.message.JmsHeaders;
import org.mule.jms.commons.api.message.JmsMessageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * com.canda.mulestac.mule4.logging.optional.JmsAttributesResolver
 */
public class LogInfoJms {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogInfoJms.class);

	/**
	 *
	 */
	public static void logJmsAttributes(Object aAttributes) {
		if (aAttributes instanceof JmsAttributes) {
			// normally is class org.mule.jms.commons.api.message.DefaultJmsAttributes (AttributesDataType=SimpleDataType{type=org.mule.jms.commons.api.message.DefaultJmsAttributes, mimeType='application/java'})
			JmsAttributes tempJmsAttributes = (JmsAttributes) aAttributes;
			JmsMessageProperties tempProperties = tempJmsAttributes.getProperties();
			Map<String, Object> tempPropertiesAsMap = tempProperties.asMap();
			for (Map.Entry<String, Object> tempEntry : tempPropertiesAsMap.entrySet()) {
				String tempVarableName = tempEntry.getKey();
				LOGGER.info("jmsProp: " + tempVarableName + "=" + tempEntry.getValue());
			}
			JmsHeaders tempHeaders = tempJmsAttributes.getHeaders();
			LOGGER.info("jmsProp:" + "getJMSCorrelationID=" + tempHeaders.getJMSCorrelationID());
			LOGGER.info("jmsProp:" + "getJMSDeliveryMode=" + tempHeaders.getJMSDeliveryMode());
			LOGGER.info("jmsProp:" + "getJMSDeliveryTime=" + tempHeaders.getJMSDeliveryTime());
			LOGGER.info("jmsProp:" + "getJMSDestination=" + tempHeaders.getJMSDestination());
			LOGGER.info("jmsProp:" + "getJMSExpiration=" + tempHeaders.getJMSExpiration());
			LOGGER.info("jmsProp:" + "getJMSMessageID=" + tempHeaders.getJMSMessageID());
			LOGGER.info("jmsProp:" + "getJMSPriority=" + tempHeaders.getJMSPriority());
			LOGGER.info("jmsProp:" + "getJMSRedelivered=" + tempHeaders.getJMSRedelivered());
			LOGGER.info("jmsProp:" + "getJMSReplyTo=" + tempHeaders.getJMSReplyTo());
			LOGGER.info("jmsProp:" + "getJMSTimestamp=" + tempHeaders.getJMSTimestamp());
			LOGGER.info("jmsProp:" + "getJMSType=" + tempHeaders.getJMSType());

		} else {
			LOGGER.info("It is not " + JmsAttributes.class + " loaded via " + JmsAttributes.class.getClassLoader());
		}
	}

}
